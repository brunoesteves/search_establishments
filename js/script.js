$(document).ready(() => {
  let allInfo = [];
  localStorage.setItem("searchTerm", "");
  localStorage.setItem("discountAmount", "noDiscount");
  localStorage.setItem("isAlphabeticall", true);
  let searchTerm = localStorage.getItem("searchTerm");

  function resultHTML(item) {
    return ` <div class="contentCompany">
              <img src="https://clube-static.clubegazetadopovo.com.br/${item.cover}" alt=${item.fantasyName} />
              <div class="infoCompany">
                <span class="nameCompany">${item.fantasyName}</span>
                <div class="discountAmount">
                   <div> Desconto: ${item.discountAmount}%</div>
                   <div><a href="#" target="_blank">Site</a><div>
                </div>
              <div>
          </div>`;
  }

  fetch("https://gdp-prd-clube.s3.amazonaws.com/api/repository/partners/all.json")
    .then((res) => res.json())
    .then((res) => {
      setAllinfo(res);
    });

  function setAllinfo(res) {
    let info = res.sort((a, b) => {
      return a.fantasyName.localeCompare(b.fantasyName);
    });
    allInfo.push(info);
    results(allInfo);
  }

  $(document).scroll(() => {
    if ($(document).scrollTop() > 300) {
      $(".filtersCompany").removeClass("filtersCompany").addClass("filtersCompanyFixedTop");
    }
  });

  $("#cleanInputFilters").on("click", () => {
    $("#searchInput").val("");
    $("#choosedDiscount option:eq(0)").prop("selected", true);
    localStorage.setItem("searchTerm", "");
    localStorage.setItem("discountAmount", "noDiscount");
    localStorage.setItem("isAlphabeticall", true);
  });

  $("#searchInput").on("input", function () {
    localStorage.setItem("searchTerm", $(this).val());
    results(allInfo);
  });

  $("#selectAlphabetically").change(function () {
    if ($("#selectAlphabetically").val() == "true") {
      localStorage.setItem("isAlphabeticall", true);
      results(allInfo);
    } else if ($("#selectAlphabetically").val() == "false") {
      localStorage.setItem("isAlphabeticall", false);
      results(allInfo);
    }
  });

  $("#choosedDiscount").change(function () {
    localStorage.setItem("discountAmount", $(this).val());
    results(allInfo);
  });

  function OrderA_Z(a, b) {
    return a.fantasyName.localeCompare(b.fantasyName);
  }

  function orderZ_A(a, b) {
    return b.fantasyName.localeCompare(a.fantasyName);
  }

  function results(allInfo) {
    let filteredList = [];
    let finalList = [];

    if (!localStorage.getItem("searchTerm") && !localStorage.getItem("discountAmount")) {
      $(".companies").html("");
      allInfo[0]
        .sort((a, b) => {
          if (localStorage.getItem("isAlphabeticall") == "true") {
            OrderA_Z(a, b);
          } else {
            return orderZ_A(a, b);
          }
        })
        .map((item) => {
          return $(".companies").append(resultHTML(item));
        });
    } else {
      let searchWord = localStorage.getItem("searchTerm").toLowerCase();
      let choosedDiscount = localStorage.getItem("discountAmount");
      $(".companies").html("");
      allInfo[0].forEach((item) => {
        if (item.fantasyName.toLowerCase().includes(searchWord)) {
          if (item.discountAmount == choosedDiscount) {
            finalList.push(item);
          } else {
            filteredList.push(item);
          }
        }
      });

      if (finalList != "") {
        $("#numberFound span").text(finalList.length);
        finalList
          .sort((a, b) => {
            if (localStorage.getItem("isAlphabeticall") == "true") {
              OrderA_Z(a, b);
            } else {
              return orderZ_A(a, b);
            }
          })
          .map((item) => {
            return $(".companies").append(resultHTML(item));
          });
      } else if (filteredList != "") {
        filteredList
          .sort((a, b) => {
            if (localStorage.getItem("isAlphabeticall") == "true") {
              OrderA_Z(a, b);
            } else {
              return orderZ_A(a, b);
            }
          })
          .map((item) => {
            return $(".companies").append(resultHTML(item));
          });
      } else {
        $(".companies")
          .html("")
          .append(`<p>Não há resultados com o termo "${localStorage.getItem("searchTerm")}"</p>`);
      }
    }
  }
});
